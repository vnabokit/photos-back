## General info

This is a backend that realizes working with simple photo gallery for certain user. This is studying project developed on Node.js and Express.js.

## Technologies

Project was created with:

-   Node.js
-   Express.js
-   Mongoose / MongoDB
-   JWT
-   Axios

## This project can:

-   Register a user in a MongoDB database by given `username`, `email` and `password`
-   Authenticate a user using JWT
-   Load mock photos from open souce (http://jsonplaceholder.typicode.com/photos) to a MongoDB database, create albums automatically
-   Get photos by `page` and `maxcount`
-   Delete photos
-   Delete albums
-   Update album's title

## Setup

### Prepare Mongo database

-   Open https://cloud.mongodb.com
-   Create a new database
-   Create 3 empty collections: `users`, `photos` and `albums`
-   Open tab _Database_, open _Connect_ window, click on _Connect your application_ button
-   Choose _Driver_ = _Node.js_, _Version_ = _4.0 or later_
-   Copy connection string and save it in some txt-file

### Install the project locally

```
$ git clone https://gitlab.com/vnabokit/photos-back.git
$ cd photos-back
$ npm install
```

### Add certificates, edit .env

-   Generate `public.pem` and `private.pem` files, put them to the `./certs` folder
-   Create `./.env` file, edit `DB_STRING` and `JWT_SECRET_KEY` variables

### Launch the project

```
$ npm run dev
```

### Usage

-   Open `http://localhost:3000/` in a web-browser. You should get responce `200` and a congratulation message
-   Open _Postman_, send POST request to `http://localhost:3000/register` with body:

```
{
    "username": "user",
    "email": "user@email.com",
    "password": "useruser"
}
```

You should get responce `201` and some confirmation message.

### List of endpoints and its HTTP methods

```
/ GET
/register POST
/login POST
/load-photos POST
/get-photos/:page/:maxcount/:ownerid? GET
/delete-photos/:photoids DELETE
/delete-albums/:albumids DELETE
/change-album-title/:albumid/:new_album_name PATCH
```

## Acknowledgments

There were such resources used while developing this project:

-   https://www.becomebetterprogrammer.com/jwt-authentication-middleware-nodejs-typescript/#Whats_Next
-   https://expressjs.com/en/guide/using-middleware.html
-   https://developer.okta.com/blog/2018/11/15/node-express-typescript
-   https://stackoverflow.com/questions/14446511/most-efficient-method-to-groupby-on-an-array-of-objects
-   ...and of course some other resources, sorry guys if I forgot to include them. Nevertheless, thanks for sharing, you are the best!
