import * as fs from 'fs';
import * as path from 'path';
import { sign, SignOptions, verify, VerifyOptions } from 'jsonwebtoken';

import { TTokenPayload } from '../types/all';

/**
 * https://www.becomebetterprogrammer.com/jwt-authentication-middleware-nodejs-typescript/#Whats_Next
 */
export function generateToken(userId: string, username: string) {
    // information to be encoded in the JWT
    const payload = {
        username,
        userId,
    };
    // read private key value
    const privateKey = fs
        .readFileSync(path.join(__dirname, './../../.certs/private.pem'))
        .toString();

    const signInOptions: SignOptions = {
        // RS256 uses a public/private key pair. The API provides the private key
        // to generate the JWT. The client gets a public key to validate the
        // signature
        algorithm: 'RS256',
        expiresIn: '1h',
    };

    const key = { key: privateKey, passphrase: process.env.JWT_SECRET_KEY };

    // generate JWT
    return sign(payload, key, signInOptions);
}

/**
 * checks if JWT token is valid
 *
 * @param token the expected token payload
 */
export async function validateToken(token: string): Promise<TTokenPayload> {
    const publicKey = fs
        .readFileSync(path.join(__dirname, './../../.certs/public.pem'))
        .toString();

    const key = { key: publicKey, passphrase: process.env.JWT_SECRET_KEY };

    const verifyOptions: VerifyOptions = {
        algorithms: ['RS256'],
    };

    return new Promise((resolve, reject) => {
        verify(
            token,
            publicKey,
            verifyOptions,
            (error, decoded: TTokenPayload) => {
                if (error) {
                    return reject(error);
                }
                resolve(decoded);
            }
        );
    });
}
