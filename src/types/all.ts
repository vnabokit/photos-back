export type TUser = {
    login: string;
    email: string;
    password: string;
    registerDate: Date;
    _id?: string;
};

export type TPhoto = {
    albumId: string;
    title: string;
    url: string;
    thumbnailUrl: string;
    owner: string;
};

export type TAlbum = {
    title: string;
    owner: string;
    _id?: string;
};

export type TTokenPayload = {
    exp: number;
    username: string;
    userId: string;
};

export type TPhotoData = {
    albumId: string;
    id: number;
    title: string;
    thumbnailUrl: string;
    owner?: string;
};

export type TAlbumOperation = {
    countPhotos: number;
    message: string;
    isSuccess: boolean;
    countAlbums?: number;
};

export type TGetPhotosParams = {
    owner?: string;
};

export type TSearchUserParams = {
    password: string;
    login?: string;
    email?: string;
};
