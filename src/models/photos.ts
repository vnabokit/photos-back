import { Schema } from 'mongoose';

import { TPhoto } from '../types/all';

const PhotosSchema = new Schema<TPhoto>({
    albumId: { type: String, required: true },
    title: { type: String, required: true },
    url: { type: String, required: true },
    thumbnailUrl: { type: String, required: true },
    owner: { type: String, required: true },
});

export default PhotosSchema;
