import { Schema } from 'mongoose';

import { TAlbum } from '../types/all';

const AlbumsSchema = new Schema<TAlbum>({
    title: { type: String, required: true },
    owner: { type: String, required: true },
});

export default AlbumsSchema;
