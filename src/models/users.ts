import { Schema } from 'mongoose';

import { TUser } from '../types/all';

const UsersSchema = new Schema<TUser>({
    login: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    registerDate: { type: Date, required: true },
});

export default UsersSchema;
