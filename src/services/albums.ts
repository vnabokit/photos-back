import { model } from 'mongoose';

import { TAlbum, TAlbumOperation, TPhotoData } from '../types/all';
import AlbumsSchema from '../models/albums';
import PhotosSchema from '../models/photos';
import { logger } from '../middleware/logger';
import * as PhotosService from '../services/photos';

export const save = async (
    userId: string,
    albumId: string,
    photos: TPhotoData[]
): Promise<TAlbumOperation> => {
    const savingResults: TAlbumOperation = {
        countAlbums: 0,
        countPhotos: 0,
        message: '',
        isSuccess: true,
    };
    const AlbumsModel = model('Albums', AlbumsSchema);
    const docAlbum = new AlbumsModel({
        title: `Album ${albumId}`,
        owner: userId,
    });
    let realAlbumId: string = '';
    try {
        const savedAlbum = await docAlbum.save();
        realAlbumId = savedAlbum._id.toString();
        savingResults.countAlbums++;
        logger.info(`Created album ID ${realAlbumId}`);
    } catch (err) {
        throw err;
    }

    photos = photos.map((photo) => {
        photo.owner = userId;
        photo.albumId = realAlbumId;
        return photo;
    });
    const PhotosModel = model('Photos', PhotosSchema);
    try {
        await PhotosModel.insertMany(photos);
        savingResults.countPhotos += photos.length;
        logger.info(`Saved ${photos.length} photos to album ID ${realAlbumId}`);
    } catch (err) {
        throw err;
    }

    return savingResults;
};

export const remove = async (
    albumIds: string[],
    ownerId: string
): Promise<TAlbumOperation[]> => {
    const Photos = model('Photos', PhotosSchema);
    const Albums = model('Albums', AlbumsSchema);
    const results: TAlbumOperation[] = [];
    for (const albumId of albumIds) {
        const removingResults = await PhotosService.removeByAlbum(
            albumId,
            ownerId
        );

        // Additionally, check if the deleting album does not have
        // photos. Because may be a situation, when some other user (non owner) has
        // photos in this album. In such case, skip removing such album.
        const photosRemained = await Photos.find({ albumId }).exec();
        if (photosRemained.length) {
            removingResults.message = `Album with ID ${albumId} still has some photos. Skip its deletion.`;
            removingResults.isSuccess = false;
            results.push(removingResults);
            continue;
        }

        let removingAlbum;
        try {
            removingAlbum = await Albums.deleteOne({
                _id: albumId,
                owner: ownerId,
            }).exec();
        } catch (err2) {
            removingResults.message = `Error while removing album id ${albumId}: ${err2.name}`;
            removingResults.isSuccess = false;
            results.push(removingResults);
            continue;
        }

        if (!removingAlbum.deletedCount) {
            removingResults.message = `No such album id ${albumId}`;
            removingResults.isSuccess = false;
            results.push(removingResults);
            continue;
        }

        removingResults.isSuccess = true;
        removingResults.countAlbums++;
        logger.info(`Deleted album ID ${albumId}`);
        results.push(removingResults);
    }

    return results;
};

export const updateTitle = async (
    albumid: string,
    owner: string,
    title: string
): Promise<boolean> => {
    const Albums = model('Albums', AlbumsSchema);
    try {
        const albumFound = await Albums.findOneAndUpdate(
            { _id: albumid, owner },
            { title }
        ).exec();
        return !!albumFound;
    } catch (err) {
        throw err;
    }
};
