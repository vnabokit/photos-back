import { model } from 'mongoose';

import { TAlbumOperation, TGetPhotosParams } from '../types/all';
import PhotosSchema from '../models/photos';
import { logger } from '../middleware/logger';

export const removeByIds = async (photoIds: string[], ownerId: string) => {
    const Photos = model('Photos', PhotosSchema);
    const photoIdsMongoose = photoIds.map((photoId) => {
        return {
            _id: photoId,
            owner: ownerId,
        };
    });
    let result;
    try {
        result = Photos.deleteMany({ $or: photoIdsMongoose }).exec();
    } catch (err) {
        throw err;
    }
    return result;
};

export const removeByAlbum = async (
    albumId: string,
    owner: string
): Promise<TAlbumOperation> => {
    const removingResult: TAlbumOperation = {
        countPhotos: 0,
        countAlbums: 0,
        message: '',
        isSuccess: true,
    };
    const Photos = model('Photos', PhotosSchema);
    let result;
    try {
        result = await Photos.deleteMany({ albumId, owner }).exec();
        removingResult.countPhotos = result.deletedCount;
        logger.info(`Deleted photos: ${result.deletedCount}`);
    } catch (err) {
        removingResult.message = `Error while removing photos of album id ${albumId}: ${err.name}`;
        removingResult.isSuccess = false;
        logger.error(removingResult.message, err);
    }
    return removingResult;
};

export const get = async (
    page: number,
    maxCount: number,
    ownerId: string | null
) => {
    const findParams: TGetPhotosParams = {};
    if (ownerId) {
        findParams.owner = ownerId;
    }
    const skip = (page - 1) * maxCount;
    const Photos = model('Photos', PhotosSchema);
    let result;
    try {
        result = await Photos.find(findParams)
            .skip(skip)
            .limit(maxCount)
            .exec();
        return result;
    } catch (err) {
        throw err;
    }
};
