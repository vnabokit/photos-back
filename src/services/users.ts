import axios from 'axios';
import { model } from 'mongoose';
import md5 from 'md5';
import * as EmailValidator from 'email-validator';

import {
    TAlbumOperation,
    TPhotoData,
    TUser,
    TSearchUserParams,
} from '../types/all';
import UsersSchema from '../models/users';
import { logger } from '../middleware/logger';
import * as AlbumsService from '../services/albums';

export const isExist = async (
    username: string,
    email: string
): Promise<boolean> => {
    const UsersModel = model('Users', UsersSchema);
    try {
        const foundUser = await UsersModel.findOne({
            $or: [{ login: username }, { email }],
        }).exec();
        return !!foundUser;
    } catch (err) {
        throw err;
    }
};

export const create = async (
    login: string,
    email: string,
    password: string
): Promise<TUser> => {
    const UsersModel = model('Users', UsersSchema);
    const doc = new UsersModel({
        login,
        email,
        password: md5(password),
        registerDate: Date.now(),
    });
    try {
        const savingResult = await doc.save();
        logger.info(savingResult);
        return savingResult;
    } catch (err) {
        throw err;
    }
};

export const get = async (
    username: string,
    password: string
): Promise<TUser | null> => {
    const searchParams: TSearchUserParams = {
        password: md5(password),
    };
    if (EmailValidator.validate(username)) {
        searchParams.email = username;
    } else {
        searchParams.login = username;
    }
    logger.debug(searchParams);
    const UsersModel = model('Users', UsersSchema);
    try {
        const user = await UsersModel.findOne(searchParams);
        logger.debug(user);
        return user;
    } catch (err) {
        console.log('error getting user:', err);
        throw err;
    }
};

export const loadPhotos = async (userId: string): Promise<TAlbumOperation> => {
    const respPhotos = await axios.get(process.env.URL_MOCK_PHOTOS);
    const photosData: TPhotoData[] = respPhotos.data;
    const photosByAlbumId = groupBy(photosData, 'albumId');
    const results: TAlbumOperation[] = [];
    for (const albumId of Object.keys(photosByAlbumId)) {
        let savingResult: TAlbumOperation;
        try {
            savingResult = await AlbumsService.save(
                userId,
                albumId,
                photosByAlbumId[albumId]
            );
        } catch (err) {
            throw err;
        }
        results.push(savingResult);
    }
    const savingResults: TAlbumOperation = {
        countAlbums: 0,
        countPhotos: 0,
        message: '',
        isSuccess: true,
    };
    return results.reduce((prev, curr) => {
        curr.countAlbums += prev.countAlbums;
        curr.countPhotos += prev.countPhotos;
        return curr;
    }, savingResults);
};

// https://stackoverflow.com/questions/14446511/most-efficient-method-to-groupby-on-an-array-of-objects
const groupBy = (xs: any, key: any) => {
    return xs.reduce((rv: any, x: any) => {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
    }, {});
};
