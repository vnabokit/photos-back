import { connect } from 'mongoose';

import { logger } from '../middleware/logger';

export const connectToDb = () => {
    connect(process.env.DB_STRING)
        .then((success) => {
            logger.info('DB connected.');
        })
        .catch((err) => {
            logger.error('Cannot connect to DB:', err);
        });
};
