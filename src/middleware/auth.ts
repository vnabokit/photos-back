import { Request, Response, NextFunction } from 'express';

import { logger } from '../middleware/logger';
import { validateToken } from './../utils/jwt';

/**
 * middleware to check whether user has access to a specific endpoint
 */
export const authorize = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    logger.info('Authorizing...');
    let jwt =
        req.headers.authorization || req.headers['x-access-token'].toString();

    if (!jwt) {
        logger.warn('Invalid token');
        return res.status(401).json({ message: 'Invalid token' });
    }
    jwt = removeBearer(jwt);
    try {
        const resu = await validateToken(jwt);
        logger.info('Token validated.');
        next();
    } catch (error) {
        if (error.name === 'TokenExpiredError') {
            logger.warn('Token is expired');
            res.status(401).json({ message: 'Expired token' });
            return;
        } else {
            logger.warn('Failed to auth');
            res.status(500).json({ message: 'Failed to authenticate user' });
            return;
        }
    }
};

export const removeBearer = (str: string): string => {
    if (str.toLowerCase().startsWith('bearer')) {
        str = str.slice('bearer'.length).trim();
    }
    return str;
};
