import 'dotenv/config';
import express, { Request, Response } from 'express';
import cors from 'cors';
import { body, param, validationResult } from 'express-validator';

import { TUser } from './types/all';
import { logger } from './middleware/logger';
import * as Auth from './middleware/auth';
import { connectToDb } from './services/db';
import * as PhotosService from './services/photos';
import * as AlbumsService from './services/albums';
import * as UsersService from './services/users';
import { generateToken, validateToken } from './utils/jwt';

const port = process.env.SERVER_PORT;
const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

connectToDb();

app.get('/', (req: Request, resp: Response) => {
    logger.info('/ endpoint triggered');
    return resp.status(200).send({message: "Hi, it is photos backend"});
});

app.post(
    '/register',
    body('username').isLength({ min: 1 }),
    body('password').isLength({ min: 1 }),
    body('email').isEmail(),
    async (req: Request, resp: Response) => {
        logger.info('/register endpoint triggered');
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return resp.status(400).send({ errors: errors.array() });
        }
        const { username, email, password } = req.body;
        const exists = await UsersService.isExist(username, email);
        if (!exists) {
            try {
                const userNew = await UsersService.create(
                    username,
                    email,
                    password
                );
                return resp.status(201).send({
                    message: `${userNew.login} is registered successfully`,
                });
            } catch (err) {
                return resp.status(500).send({
                    message: `Error while saving new user: ${err.name}`,
                });
            }
        } else {
            return resp.status(400).send({ message: 'User already exists' });
        }
    }
);

app.post(
    '/login',
    body('login').isLength({ min: 1 }),
    body('password').isLength({ min: 1 }),
    async (req: Request, resp: Response) => {
        logger.info('/login endpoint triggered');
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return resp.status(400).send({ errors: errors.array() });
        }
        let user: TUser | null;
        try {
            user = await UsersService.get(req.body.login, req.body.password);
        } catch (err) {
            return resp.status(500).send({
                message: `Error while getting user: ${err.name}`,
            });
        }
        if (user) {
            const token = generateToken(user._id.toString(), user.login);
            return resp.status(200).send({
                accessToken: token,
                username: user.login,
                email: user.email,
            });
        } else {
            return resp
                .status(401)
                .send({ message: 'Wrong user name or password' });
        }
    }
);

app.post(
    '/load-photos',
    Auth.authorize,
    async (req: Request, resp: Response) => {
        logger.info('/load-photos endpoint triggered');
        const decodedToken = await validateToken(
            Auth.removeBearer(req.headers.authorization)
        );
        try {
            const loadingResult = await UsersService.loadPhotos(
                decodedToken.userId
            );
            return resp.status(201).send({
                message: 'Mock photos are loaded',
                result: loadingResult,
            });
        } catch (err) {
            return resp.status(500).send({
                message: `Failed to load mock photos: ${err.name}`,
            });
        }
    }
);

app.get(
    '/get-photos/:page/:maxcount/:ownerid?',
    param('page').isInt({ min: 1, max: 99999 }),
    param('maxcount').isInt({ min: 1, max: 99999 }),
    async (req: Request, resp: Response) => {
        logger.info('/get-photos endpoint triggered');
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return resp.status(400).send({ errors: errors.array() });
        }
        let photos;
        try {
            photos = await PhotosService.get(
                parseInt(req.params.page, 10),
                parseInt(req.params.maxcount, 10),
                req.params.ownerid ? req.params.ownerid : null
            );
            return resp.status(200).send({
                message: 'Photos are fetched succesfully',
                result: photos,
            });
        } catch (err) {
            return resp.status(500).send({
                message: 'Error while fetching photos: ' + err.name,
            });
        }
    }
);

app.delete(
    '/delete-photos/:photoids',
    param('photoids').isLength({ min: 1 }),
    Auth.authorize,
    async (req: Request, resp: Response) => {
        logger.info('/delete-photos endpoint triggered');
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return resp.status(400).send({ errors: errors.array() });
        }
        const photoIds: string[] = req.params.photoids
            ? req.params.photoids.split(',')
            : [];
        if (!photoIds.length) {
            return resp.status(400).send({ message: 'Nothing to delete' });
        }
        const decodedToken = await validateToken(
            Auth.removeBearer(req.headers.authorization)
        );

        try {
            const result = await PhotosService.removeByIds(
                photoIds,
                decodedToken.userId
            );
            return resp.status(200).send({
                message: `${result.deletedCount} photos are deleted`,
            });
        } catch (err) {
            logger.info('Error while deleting photos:', err);
            return resp.status(500).send({
                message: `Error while deleting photos: ${err.name}`,
            });
        }
    }
);

// If at least one album was deleted succesfully,
// then concern it as succesfull deletion (status 201).
app.delete(
    '/delete-albums/:albumids',
    param('albumids').isLength({ min: 1 }),
    Auth.authorize,
    async (req: Request, resp: Response) => {
        logger.info('/delete-albums endpoint triggered');
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return resp.status(400).send({ errors: errors.array() });
        }
        const albumIds: string[] = req.params.albumids
            ? req.params.albumids.split(',')
            : [];
        if (!albumIds.length) {
            return resp.status(400).send({ message: 'Nothing to delete' });
        }

        const decodedToken = await validateToken(
            Auth.removeBearer(req.headers.authorization)
        );
        let results;
        try {
            results = await AlbumsService.remove(albumIds, decodedToken.userId);
        } catch (err) {
            return resp.status(500).send({
                message: `Error while deleting albums: ${err.name}`,
            });
        }
        let isAtLeastOneDeleted: boolean;
        for (const oneResult of results) {
            isAtLeastOneDeleted ||= oneResult.isSuccess;
        }
        if (isAtLeastOneDeleted) {
            return resp.status(200).send({
                message: `Albums are deleted`,
                result: results,
            });
        } else {
            return resp.status(500).send({
                message: `Albums are not deleted`,
                result: results,
            });
        }
    }
);

app.patch(
    '/change-album-title/:albumid/:new_album_name',
    param('albumid').isLength({ min: 1 }),
    param('new_album_name').isLength({ min: 1 }),
    Auth.authorize,
    async (req: Request, resp: Response) => {
        logger.info('/change-album-title endpoint triggered');
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return resp.status(400).send({ errors: errors.array() });
        }
        const decodedToken = await validateToken(
            Auth.removeBearer(req.headers.authorization)
        );
        let isUpdated;
        try {
            isUpdated = await AlbumsService.updateTitle(
                req.params.albumid,
                decodedToken.userId,
                req.params.new_album_name
            );
        } catch (err) {
            return resp.status(400).send({
                message: `Failed renaming album: ${err.name}`,
            });
        }
        if (isUpdated) {
            return resp.status(201).send({
                message: 'Album was renamed',
            });
        } else {
            return resp.status(400).send({
                message: 'Album was not renamed',
            });
        }
    }
);

app.listen(port, () => {
    logger.info('Server is launched on port 3000.');
});
